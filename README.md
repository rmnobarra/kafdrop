# Kafdrop + nginx as reverse proxy with authentication

# Prereq

apache2-utils for Ubuntu or httpd-tools for Centos systems in order to
generate admin's password

---

## Kafdrop

1. Adjust  KAFKA_BROKERCONNECT env with your cluster values.

---

## Nginx

2. Generate admin password (default is admin)

```bash
htpasswd -c conf/nginx/.htpasswd admin
```

3. Run Forrest run

```bash
docker-compose up -d
```

Ref: https://github.com/obsidiandynamics/kafdrop